unit uRefs;

interface
uses
  Kol,
  uLang,
  uVars,
  SZCodeBaseX,
  smtpsend,
  mimemess,
  //mimepart,
  synachar,
  synautil,
  windows,
  SimpleXML;


var
  //Global Vars
  slConnections : PStrList;  // List of connection strings
  slMagazine    : PStrList;  // List of connection magazins
  slCash        : PStrList;  // List of magazin's cashs
  slResultMail  : PStrList;  // List for result mail

  xmlResult     : IXmlDocument;

  lsSQL : PStrList;
  lsSrPos : PStrList;

function urGetHostFromConnection(ConString : string): string;
function urStrReplaceChar(const S: AnsiString; const Source, Replace: AnsiChar): AnsiString;
procedure urDebugWrite(str: string);
function urSendDataToMail: Boolean;
function urSendXMLToMail: Boolean;
function urReadParams : Byte;
procedure urInitVars;
procedure urFillSQLScript(cCash: ShortString);
function urReadConfig(ConfigFile: string): Boolean;
function urWriteHeadInformation : IXmlDocument;
procedure urWriteDataInfo(dat : TDateTime; Mag : ShortString; Sum : Double; Cheque, AvgCheque : Integer);
function urSendXMLData: Boolean;
function TryUpdateConfig(cf : IXmlDocument): IXmlDocument;
procedure MyDummyOleError( Result: HResult );

implementation
uses
  Classes, SysUtils;


function urGetHostFromConnection(ConString : string): string;
const ds = 'Data Source=';
var s : string;
begin
  s := ConString;
  s := Copy(s,Pos(ds,s)+length(ds),Length(s)-Pos(ds,s)+length(ds));
  s := Copy(s,1,Pos(';',s)-1);
  Result := s;
end;

function urStrReplaceChar(const S: AnsiString; const Source, Replace: AnsiChar): AnsiString;
var
  I: Integer;
begin
  Result := S;
  for I := 1 to Length(S) do
    if Result[I] = Source then
      Result[I] := Replace;
end;

procedure urDebugWrite(str: string);
begin
  if DebugMode then begin
    AssignFile(DebugFile, ExtractFilePath(ParamStr(0))+ 'debug.log');
    if FileExists(ExtractFilePath(ParamStr(0))+ 'debug.log') then Append(DebugFile)
                                                             else Rewrite(DebugFile);
    Writeln(DebugFile, DateTime2StrShort(Now) + ': ' + str);
    CloseFile(DebugFile);
  end;
end;

function urSendDataToMail: Boolean;
var
  slMessage : TStringList;
begin
  slMessage := TStringList.Create;
  slMessage.Clear;
  slMessage.Add('From: '+(*'Mail Z'*)sRegionName+' <'+sEmailFrom+'>');
  slMessage.Add('To: ' +sEmailTo);
  slMessage.Add('Date: '+Rfc822DateTime(Now()-VCLDate0));
  slMessage.Add('Subject: =?windows-1251?B?' + SZEncodeBase64('Mail Z �� ' + sRegionName + ' �� ' + Date2StrFmt('dd.MM.yyyy',Date() - DateZ - 1)) + '?=');
  slMessage.Add('X-Mailer: '+ AppName);
  slMessage.Add('X-Priority: 2 (High)');
  slMessage.Add('MIME-Version: 1.0');
  slMessage.Add('Content-Type: text/plain;');
  slMessage.Add(#9'charset=windows-1251');
  slMessage.Add('Content-Transfer-Encoding: 8bit');
  slMessage.Add('');
  slMessage.Append(slResultMail.Text);
  slMessage.Add('-- ');
  slMessage.Add(ShortName + ' ' + AppVers);
  urDebugWrite('������ ������� � ��������:'+chr(10)+chr(13)+slMessage.Text);
  Result := SendToRaw(sEmailFrom,sEmailTo,sMailServer,slMessage,sMailUser,sMailPass);
  if Result then urDebugWrite('������ ������� ����������.')
            else urDebugWrite('������ �� ����������');
end;

function urSendXMLToMail: Boolean;
begin
  Result := urSendXMLData;
end;

function urReadParams : Byte;
begin
  Result := 0;
  try
    if ParamCount > 0 then DateZ := Str2Int(ParamStr(1))
    else DateZ := 0;
    DebugMode := UpperCase(Trim(ParamStr(2)))='DEBUG';
  except
    Result := 1;
  end;
end;

procedure urInitVars;
begin
    slConnections := NewStrList;
    slConnections.Clear;
    slCash := NewStrList;
    slCash.Clear;
    slMagazine := NewStrList;
    slMagazine.Clear;
    slResultMail := NewStrList;
    slResultMail.Clear;
    lsSQL := NewStrList;
    lsSrPos := NewStrList;
end;

procedure urFillSQLScript(cCash: ShortString);
begin
  //������� � �������� �� ����
  lsSQL.Clear;
  lsSQL.Add('SET DATEFORMAT mdy');
  lsSQL.Add('SELECT');
  lsSQL.Add(' CashNumber AS CASHNUMBER,');
  lsSQL.Add(' GangNumber AS ZNUMBER,');
  //lsSQL.Add(' CONVERT(datetime, CAST(MONTH(GangDateStop) AS CHAR)+''/''+CAST(DAY(GangDateStop) AS CHAR)+''/''+CAST(YEAR(GangDateStop) AS CHAR),101) AS SDATE,');
  lsSQL.Add(' Summa AS SUMMA,');
  lsSQL.Add(' SummaRetCash As SummaVozvrat,');
  lsSQL.Add(' ChequeCountSale AS ChequeSale,');
  lsSQL.Add(' ChequeCountRet AS ChequeVozvrat');
  lsSQL.Add('FROM');
  lsSQL.Add(' OperGang');
  lsSQL.Add('WHERE');
//    lsSQL.Add(' (OperDay = '''+Trim(Date2StrFmt('yyyyMMdd',Date()-1))+''')');
  lsSQL.Add(' (GangDateStop BETWEEN '''+Trim(Date2StrFmt('yyyy-MM-dd',Date()-DateZ-1))+' 02:00:00'' AND '''+Trim(Date2StrFmt('yyyy-MM-dd',Date()-DateZ))+'  02:00:00'')');
  if Trim(cCash) <> '' then lsSQL.Add(' AND (CashNumber in ('+cCash+'))');
//    lsSQL.Add(' (GangDateStop > '''+Trim(Date2StrFmt('yyyy-MM-dd',Date()-1))+''')');
  lsSQL.Add('ORDER BY  CASHNUMBER ASC, ZNUMBER');
//  lsSQL.Add('ORDER BY  SDATE ASC,CASHNUMBER ASC, ZNUMBER');

  //������� ���������� ������� � ����
  lsSrPos.Clear;
  lsSrPos.Add('select avg(d.check1) as SrCheque from (');
  lsSrPos.Add('    SELECT count(*) as check1 FROM ChequePos');
  lsSrPos.Add('    where ChequeId in (');
  lsSrPos.Add('        SELECT t.chid from (');
  lsSrPos.Add('            select og.CashNumber, og.GangNumber, og.GangDateStop, ch.id chid, ch.Operation, ch.NSmena, ch.Ck_Number');
  lsSrPos.Add('            from');
  lsSrPos.Add('              OperGang as og');
  lsSrPos.Add('             left join chequehead ch on (ch.NSmena = og.GangNumber) and (ch.Cash_code = og.CashNumber)');
  lsSrPos.Add('            where og.GangDateStop between  '''+Trim(Date2StrFmt('yyyy-MM-dd',Date()-DateZ-1))+' 02:00:00'' AND '''+Trim(Date2StrFmt('yyyy-MM-dd',Date()-DateZ))+'  02:00:00''');
  if Trim(cCash) <> '' then lsSrPos.Add(' AND (og.CashNumber in ('+cCash+'))');
  lsSrPos.Add('        ) as t');
  lsSrPos.Add('    )');
  lsSrPos.Add('    group by chequeid');
  lsSrPos.Add(') as d');
end;

function urReadConfig(ConfigFile: string): Boolean;
var
  doc    : IXmlDocument;
  xnl    : IXmlNodeList;
  xnd    : IXmlNode;
  i      : Byte;
begin
  doc := CreateXmlDocument();
  try
    try
      doc.Load(ConfigFile);
      urDebugWrite(urTranslateString('��������� �������.'));
      if not doc.DocumentElement.AttrExists('version') then begin
        doc := TryUpdateConfig(doc);
        doc.Save(ConfigFile);
      end;
      xnd         := doc.DocumentElement.SelectSingleNode('Options');
      Lang := xnd.GetChildText('lang','ru');
      xnd := doc.DocumentElement.SelectSingleNode('SMTPMail');
      sMailServer := xnd.GetChildText('SMTPServer','smtp.mail.ru');
      urDebugWrite(urTranslateString('�������� ������: ')+ sMailServer);
      sMailPort   := xnd.GetChildText('SMTPPort','25');
      urDebugWrite(urTranslateString('�������� ����: ')+ sMailPort);
      sMailUser   := xnd.GetChildText('SMTPUser','test@mail.ru');
      urDebugWrite(urTranslateString('�������� ������������: ')+ sMailUser);
      sMailPass   := xnd.GetChildText('SMTPPass','test');
      urDebugWrite(urTranslateString('�������� ������: ****** (� �� ��� �������?)'));
      sEmailFrom  := xnd.GetChildText('SMTPEmail','test@mail.ru');
      urDebugWrite(urTranslateString('�������� email: ')+sEmailFrom);
      sRegionName := xnd.GetChildText('SMTPFromRealName','Real Name');
      urDebugWrite(urTranslateString('��� ������� � ���� ���������: ') + sRegionName);
      //sFullReport := xnd.GetChildText('REPORT','SMALL');
      //urDebugWrite(urTranslateString('����������� ����������: ') + sFullReport);
      xnl         := doc.DocumentElement.SelectNodes('ConnectionStrings');
      for i := 1 to xnl.Count do begin
        xnd := xnl.Item[i-1];
        slConnections.Add(xnd.Values['']);
        slMagazine.Add(xnd.GetAttr('Name',''));
        slCash.Add(xnd.GetAttr('Cashes',''));
      end;
      urDebugWrite(urTranslateString('��������� �����������'));

      xnl         := doc.DocumentElement.SelectNodes('MailTo');
      sEmailTo    := '';
      for i := 1 to xnl.Count do begin
        xnd := xnl.Item[i-1];
        sEmailTo  := sEmailTo + xnd.GetAttr('RealName','') + ' <' + xnd.Values[''] + '>, ';
      end;
      if sEmailTo <> '' then Delete(sEmailTo,Length(sEmailTo)-1,2);
      urDebugWrite(urTranslateString('������ ���������: ')+ sEmailTo);
      Result := True;
    except
      Result := False;
    end;
  finally
    xnd := nil;
    doc := nil;
  end;
end;

function urWriteHeadInformation : IXmlDocument;
var
  xhead : IXmlNode;
begin
  try
    Result := CreateXmlDocument('mailz','1.0','win-1251');
    xhead := Result.AppendElement('headinfo');
    xhead.SetChildText('version', AppVers);
    xhead.SetChildText('from',sEmailFrom);
    xhead.SetChildText('date',Date2StrFmt('dd.MM.yyyy',Date()));
  finally
    xhead := nil;
  end;
end;

procedure urWriteDataInfo(dat : TDateTime; Mag : ShortString; Sum : Double; Cheque, AvgCheque : Integer);
var
  xnode : IXmlNode;
begin
  try
    xnode := xmlResult.AppendElement('objdata');
    xnode.SetDateTimeAttr('date',dat);
    xnode.SetFloatAttr('sum',Sum);
    xnode.SetIntAttr('cheque',Cheque);
    xnode.SetIntAttr('avgcheque', AvgCheque);
    xnode.Text := Mag;
  finally
    xnode := nil;
  end;
end;

function urSendXMLData: Boolean;
var
  mm : TMimeMess;
  //mp : TMimePart;
  ms : TMemoryStream;
begin
  Result := false;
  try
     ms := TMemoryStream.Create;
     xmlResult.Save(ms);

     mm := TMimeMess.Create;
     mm.Header.ToList.Text := sEmailTo;
     mm.Header.From := sEmailFrom;
     mm.Header.Subject := AppName + ' XML Version';
     mm.Header.Date := Now - VCLDate0;
     mm.Header.XMailer := ShortName;
     mm.Header.Priority := MP_high;
     mm.Header.CharsetCode := CP1251;
     //mp := mm.AddPartMultipart('mixed',nil);
     mm.AddPartBinary(ms,'mailza.xml',nil);

     mm.EncodeMessage;

     Result := SendToRaw(sEmailFrom,sEmailTo,sMailServer+':'+sMailPort,mm.Lines,sMailUser,sMailPass);
  finally
    FreeAndNil(mm);
    //FreeAndNil(mp);
    FreeAndNil(ms);
  end;
end;

function TryUpdateConfig(cf : IXmlDocument):IXmlDocument;
begin
  Result := cf;
end;  

procedure MyDummyOleError( Result: HResult );
begin
  Result := 0;
end;


end.
