unit uVars;

interface

const
  ShortName = 'MailZ Mailer';
  AppVers = '2.0';
  AppName = ShortName + ' ' + AppVers + ' � Kirill Krasnov';

const
  cmfFullFormat  = 'FULL';
  cmfXMLFormat   = 'XML';
  cmfSmallFormat = 'SMALL';

const
  DefaultXMLText =
  '<code>'+#10#13+
  '   <query>SET DATEFORMAT mdy'+#10#13+
  '           CashNumber AS CASHNUMBER,'+#10#13+
  '           GangNumber AS ZNUMBER,'+#10#13+
  '           Summa AS SUMMA,'+#10#13+
  '           SummaRetCash As SummaVozvrat,'+#10#13+
  '           ChequeCountSale AS ChequeSale'+#10#13+
  '           ChequeCountRet AS ChequeVozvrat'+#10#13+
  '          FROM'+#10#13+
  '           OperGang'+#10#13+
  '          WHERE'+#10#13+
  '          '+#10#13+
  '          ORDER BY  CASHNUMBER ASC, ZNUMBER'+#10#13+
  '';


var
  DateZ         : Integer;   // Z from Date

var

  Lang          : ShortString; // Language code: default = ru

  sRegionName,
  sFullReport,
  sEmailTo,
  sMailServer,
  sMailPort,
  sEmailFrom,
  sMailUser,
  sMailPass            : string;


  DebugMode            : Boolean;
  DebugFile            : TextFile;

  MailFormat           : ShortString;



implementation

end.
