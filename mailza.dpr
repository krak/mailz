(*
    MailZ - program for sending via smtp server day's sells taken from crystall
    set.

    MailZ - ��������� ��� �������� ������ �������� ����� ��������� smtp-������.

    https://bitbucket.org/krak/mailz/
    Author: Kirill Krasnov kirillkr (at) gmail.com

    Uses: Synapse Lib http://www.ararat.cz/synapse
          Kol Lib http://www.kolnmck.ru
          SimpleXML https://github.com/kirill/simplexml
          SZCodeBaseX https://bitbucket.org/krak/szutils
*)

program mailza;
uses
  uRefs in 'src\uRefs.pas',
  uLang in 'src\uLang.pas',
  uVars in 'src\uVars.pas',
  err in 'library\kol\err.pas',
  Kol in 'library\kol\Kol.pas',
  KolEdb in 'library\kol\Addons\KolEdb.pas',
  SZCodeBaseX in 'library\szutils\SZCodeBaseX.pas',
  SimpleXML in 'library\simplexml\SimpleXML.pas',
  mimemess in 'library\synapse\mimemess.pas',
  blcksock in 'library\synapse\blcksock.pas',
  smtpsend in 'library\synapse\smtpsend.pas',
  mimeinln in 'library\synapse\mimeinln.pas',
  mimepart in 'library\synapse\mimepart.pas',
  synachar in 'library\synapse\synachar.pas',
  synautil in 'library\synapse\synautil.pas',
  synafpc in 'library\synapse\synafpc.pas',
  synacode in 'library\synapse\synacode.pas',
  synaicnv in 'library\synapse\synaicnv.pas',
  synsock in 'library\synapse\synsock.pas',
  synaip in 'library\synapse\synaip.pas';

{$R resource\versioninfo.res}

var
  DS: PDataSource;
  SS: PSession;
  QR: PQuery;





var
  //Local variables
  i       : Byte;
  sCurCon : string;  // Current connection string
  sCurCash: ShortString; // Current cash list of magazine
  a,b     : Double;
  c,d,e   : Integer;
begin
  OleError := MyDummyOleError;
  urReadParams;
  try
    urDebugWrite('����� ���������... ');
    Applet := NewApplet('MailZ Alone');

    urInitVars;
    
    urDebugWrite('������� ��������� ���� ��������...');
    if not urReadConfig(ExtractFilePath(ParamStr(0))+'mailza.xml') then exit;
    MailFormat := UpperCase(sFullReport);

    if MailFormat = cmfXMLFormat then begin
      xmlResult := urWriteHeadInformation;
    end;

    for i := 1 to slConnections.Count do begin
      sCurCon  := slConnections.Items[i-1];
      sCurCash := slCash.Items[i-1];
      urDebugWrite('����������: '+ sCurCon);
      a := 0;
      b := 0;
      c := 0;
      d := 0;
      try
        DS := NewDataSource(sCurCon);
        if DS <> nil then SS := NewSession(DS);
        if SS <> nil then QR := NewQuery(SS);
        if QR <> nil then begin
          urFillSQLScript(Trim(sCurCash));
          urDebugWrite('����� ������� �������������: ');
          urDebugWrite(lsSql.Text);
          urDebugWrite('����� ������� �������� ���������� ������� � ����: ');
          urDebugWrite(lsSrPos.Text);
          QR.Text := lsSQL.Text;
          QR.Mode := rmReadOnly;
          urDebugWrite('���������� ������� �������������.');
          QR.Open;
          while not QR.EOF do begin
            a := a + QR.RField[2];//a := a + QR.RField[3];
            b := b + QR.RField[3];//b := b + QR.RField[4];
            c := c + QR.IField[4];//c := c + QR.IField[5];
            d := d + QR.IField[5];//d := d + QR.IField[6];
            QR.Next;
          end;
          QR.Text := lsSrPos.Text;
          QR.Mode := rmReadOnly;
          urDebugWrite('���������� ������� �������� ���������� ������� � ����.');
          QR.Open;
          e := 0;
          while not QR.EOF do begin
            e := e + QR.IField[0];
            QR.Next;
          end;
          if MailFormat = cmfFullFormat then begin
            slResultMail.Add('������ � ' + slMagazine.Items[i-1]);
            slResultMail.Add('����� ����������: '+urStrReplaceChar(Double2Str(a),'.',',')+' '#9' ����� ��������: '+urStrReplaceChar(Double2Str(b),'.',','));
            slResultMail.Add('���������� �����: '+Int2Str(c+d)+ '. �� ���: ���������� '+ Int2Str(c)+' ������� '+Int2Str(d)+'.');
            slResultMail.Add('������� ���������� ������� � ����: '+Int2Str(e));
            //slResultMail.Add(StrReplaceChar(Double2Str(a),'.',',')+Chr(39)+Int2Str(c));
            slResultMail.Add('');
          end
          else if MailFormat = cmfSmallFormat then begin
            // ���������� �� ������ 0000252  // ��������� �� ������ 0000301
            slResultMail.Add(Date2StrFmt('dd.MM.yyyy',Date() - DateZ - 1) + ' ' + slMagazine.Items[i-1] + ' '+ urStrReplaceChar(Double2Str(a),'.',',') + ' ' + Int2Str(c) + ' ' + Int2Str(e));
          end
          else if MailFormat = cmfXMLFormat then begin
            urWriteDataInfo(Date() - DateZ - 1,slMagazine.Items[i-1],a,c,e);
          end
          else urDebugWrite('�������� ������ ����������: '+ MailFormat);
          urDebugWrite('������ �������� � ���������.');
        end
        else begin
          if (MailFormat = cmfSmallFormat) or (MailFormat = cmfSmallFormat) then begin
             slResultMail.Add('������� ' + slMagazine.Items[i-1] + ' �� ��������.');
             slResultMail.Add('');
          end;
          if (MailFormat = cmfXMLFormat) then begin
             //������ ������ �� �����.
          end;
          urDebugWrite('������ �� ��������. ��������� ��������.');
        end;
      finally
        Free_And_Nil(QR);
        Free_And_Nil(SS);
        Free_And_Nil(DS);
      end;
    end;  //begin i
    if MailFormat = cmfXMLFormat then begin
      if not urSendXMLData then MsgOK('������ �������� ������!');
    end;
    if (MailFormat = cmfFullFormat) or (MailFormat = cmfSmallFormat) then begin
        if not urSendDataToMail then MsgOK('������ �������� ������!');
    end;
  finally
    xmlResult := nil;
    Free_And_Nil(slConnections);
    Free_And_Nil(slMagazine);
    Free_And_Nil(slCash);
    Free_And_Nil(slResultMail);
    Free_And_Nil(lsSQL);
    Free_And_Nil(lsSrPos);
  end;
end.
